# Running wordpress sites with docker
Efficient developing and serving of wordpress sites. 

This project is intended to be helpful for making wordpress pleasant to develop, as well as serve in an easy and secure mode in production.

The concept is that each site runs in it's own Docker container, and keeps it's own sql database in MariaDb DBMS, runing in a docker container too. Access to the sites is from the public internet provided through NginX as a reverse proxy: configured as a TLS termintaion node.

I also have documented and provided possibilities to extend TLS to all the backend Inter Process Communication IPC using a locally created Certificate Authority (including between the site instances and the DBMS).

Tools and workflows are documented to make it simple to:
- deploy dummy certificates for local development;
- generate the required certificates and signatures to be your own local CA;
- scheduled requests to LetsEncrypt to sign and make genuine the certificates in production;
- scheduled updates of each wordpress site (plugins, themes, core, and core-db);
- scheduled full backups of the wordpress sites and databases;
- using wordpress child themes as a workflow for site development;

Tech:
- Docker and docker-compose;
- MariaDb;
- NginX;
- Certbot;
- Wordpress CMS;
- OpenSSL;
Many thanks to all the people who produce, develop, and maintain these fantastic technologies!

Dependencies:
For the TLS technology such as the LocalCA, OpenSSL and LetsEncrypt interactions, there are two dedicated repositories: `weleoka/tls-certs-int`, and `weleoka/tls-certs-ext`. These automation processes and scripts, as well as dedicated documentation are shared on request.


# TODO

- Ensure the Dockerfiles are supplied with the systems EastRSA root CA certificate;
- Make sure that phpsite uses TLS to MariaDb;
- Test the production mode of `init-tl-ext.sh`




# Getting started, all brand new
There are three files need to be modified to allow you site to set up, these are `conf/mariadb/init_sites.sql`, `docker-compose.yml`, and `.env` Math up the passwords and names as required.

Start the stack: `docker-compose up --build`. If it's the first time starting then `MYSQL_RANDOM_ROOT_PASSWORD` will output the MariaDB root password to `stdout`, it can be worth keeping that password for later use.



# Getting started, importing existing
If you have a wordpress site and it is not running in docker
https://codeblog.dotsandbrackets.com/migrate-wordpress-docker/
```
# Individual db
mysqldump -p -u root wordpress2_db > wordpress2_db.sql
# All db's
mysqldump -p -u root --all-databases > all_databases.sql
# Individual site source
tar -czf wordpress2.tar.gz /var/www/html/wordpress2
# All sites source
tar -czf all_sites.tar.gz /var/www/html
# Use Secure Copy: scp
```
The opposite of mysqldump: `mysql -p -u root <database_name> < wordpress2_db.sql`. Alternatively if logged in, then create the database, then run `source <filename.sql>`, which will read from current directory in filesystem.


Make sure you know your MariaDb root password, or if the MariaDb container is run for the first time and you have made the desired additions to `conf/build-contexts/mariadb/init-sites.sql` then you can use that user. However it's still recommended to have the root password safe somewhere. A note on the MariaDb root user: ideally change the name of 'root' to something non-standard: this because brute-force can be run pretty spectacularly fast against the database auth system.
  
- add mysql database user with password and create the new database;
- set volume name to phpsite and nginx bindmounts;
- bindmount the source to phpsite container (to nginx it will be included with all sites);
- create sites-available .conf and add symbolic link to in in sites-enabled;
- bindmount to mariadb the required database dump files to load db;
- enter mariadb container `docker-compose run --no-deps mariadb --entrypoint=ash`;
- feed in the dump file `mysqldump -p -u root my-database-name < dumpfile.sql` (match name here to existing DB);
- modify your sites `wp-config.php` database hostname, database name, user, and passwords;

GENERATED ROOT PASSWORD: oush8Maa0Da4uxuj4pahd5cahPae0goo
mysqldump -p -u root kelsall39_db < ./site_dbs/wpdb_trimaran.sql



## Enable debug for wordpress development
In the `Dockerfile` for `phpsite` switch between copying `php.ini-development`, or `php.ini-production` to the `php.ini` file.


```
// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );
// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', false );
// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', true );
# Make the PHP engine output errors.
ini_set( 'display_errors', 1 );

// As a SECURITY note if yu are only editing the backend way:
define('DISALLOW_FILE_EDIT', true);


# Try a few of these tools
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

```


## Editing with permissions issues
find . -type f -exec chmod 666 {} \;
find . -type d -mindepth 1 -exec chmod 777 {} \;










## Details on `init-sites.sql`
Whatever we mount to `/docker-entrypoint-initdb.d` path inside of MariaDb container will be executed when container starts for the first time, if you look at `docker-compose.yml` you can see that we mount `init-sites.sql` to there. 

However, if adding or changing the credentials then there is a different process.



# Tools and workflows

## Wp-cli
A recommended tool for working with the wordpress installations is WP-cli. Download it to the tools folder where it will be mounted by the wordpress sites: 
```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar ./conf/build-contexts/wordpress
```
See more at: https://make.wordpress.org/cli/handbook/guides/installing/

To use WP-cli in a container, make sure it is up and running with the database container too, then enter it with `docker exec -itu www-data phpsite1 bash`, from there it should be possible to run the various Wp-cli commands, like for example: `wp core update`, or `wp theme update --all`.

### Install WP with Wp-cli
Recommended then is to install the wordpress instance using the wp command. For example if the container is called wordpress1 then: 
```
docker exec -itu www-data wordpress1 wp core install
With required parameters:
 --url parameter (The address of the new site.)
 --title parameter (The title of the new site.)
 --admin_user parameter (The name of the admin user.)
 --admin_email parameter (The email address for the admin user.)
```

### Update WP with a script
Run the script `wp-docker-update.sh` to update all the wordpress installations.

Q: can we update the wordpress installations from "outside" the containers? If the host has php installed, can we then update the site files directly as they exist on host mounted volume?

### Backup WP with a script
Todo: WORK IN PROGRESS

## Establishing a child theme
I wont go into the details here but it is simply a matter of creating a child theme directory, then adding a `style.css` containing some metadata: what is the parent theme; name, version, etc. The child theme also needs a `fuctions.php`. See example provided in repo, and more here: https://developer.wordpress.org/themes/advanced-topics/child-themes/ or https://tubemint.com/child-theme/

Short summary:
```
mkdir twentytwenty-child
cd twentytwenty-child
touch style.css
# Insert required content
touch functions.php
# Insert required function
cd ..
zip -r twentytwenty-child.zip twentytwenty-child/
# Install the theme via wordpress admin interface
```
The zip can now be uploaded using wordpress admin interface, or installed using wp-cli. Another option is to host it in a git-repository as a zip and access it by public URI from wp-cli. https://developer.wordpress.org/cli/commands/theme/install/

To copy the child theme folder to the `wordpress1/wp-content/themes` directory is possible, but it can be difficult to maintain the correct permissions, the first zip then upload to admin interface route is pretty easy.

It's then possible to edit the files directly in `wordpress-docker/wordpress1/wp-content/themes/twentytwenty-child`, but again beware of permissions issues.

## Hardening the WP installation
This is a security discussion which is not documented here at this point, but can be shared on request.

## Production with PHP-FPM
Good docker resource for this job: https://github.com/markhilton/docker-php-fpm

It may be worthwhile going as a FPM solution instead of Apache for powering Wordpress CMS. The `conf/wordpress/Dockerfile-fpm` is using such a base image. Different Dockerfiles for each site is a possibility. 

Differences is that there are no port mappings from a FMP container as it will bind to unix sockets instead. This requires an NginX server (can of course be in a container too) to exist on the same host to read and write that same Unix socket that PHP-FPM is using. Again, here we have to make double sure that permissions (GID and UID) will be ompatible because we have different containers sharing the same "file" on the host system.

The Wordpress Docker images for fpm have the onfiguration file in `./usr/local/etc/php-fpm.conf`, and we see also that `include=etc/php-fpm.d/*.conf` will be read, full path that translates to `/usr/local/etc/php-fpm.d/*.conf`.

## Updating sites running in Production
A solution is to run updates to a site in one place, and only deploy the tested and working site (in which case Wp-cli doesn't have to be in the production container).

## Editing your theme from host machine
To do this you would have to make sure that the files are created with the right permissions which is easiest done on the host machine: `sudo -u www-data bash` and then navigate to the right site, e.g. `wordpress-docker/wordpress1/wp-content/themes/twentytwenty-child`. 

Sooner or later during edit of theme you'll have to simply add your user to the `www-data` group. 

`usermod -a -G www-data youruser`, then unfortunatley you'll have to log out and log in again. It should be possible to copy and edit theme files from then on.




# PHP-FPM and TLS connections to MariaDb
This is partly a test looking to see if the EasyRSA ca.crt is trusted `/etc/ssl/certs/ca-certificates.crt` in the phpfpm/phpsiteX containers. 

First, enable SSL for user. This seems to break the connection for wordpress. `GRANT ALL PRIVILEGES ON wordpress2_db.* TO 'wordpress2'@'%' REQUIRE SSL;` with`REQUIRE SSL` site can't connect to DB. Without, but with the `define('MYSQL_CLIENT_FLAGS', MYSQLI_CLIENT_SSL);` in `wp-config.php` things work as expected, after the wp install process (before that it is not necessarily TLS).


```
210124 20:59:27      3 Connect  wordpress2@172.24.26.1 on  using TCP/IP
210124 21:07:27     15 Connect  wordpress2@172.24.26.1 on  using SSL/TLS
```

``` 
/** Enable TLS from PHP to MariaDb */
define('MYSQL_CLIENT_FLAGS', MYSQLI_CLIENT_SSL);
```

Test with password: 'wordpresspw':
`mysql --ssl -u wordpress2 -h mariadb -p`
`sql> show global variables like '%ssl%';`
`sql> status;` 

Test with PHP:
```
php -a
php> $db = mysqli_init();
php> mysqli_real_connect($db, 'mariadb', 'wordpress2', 'wordpresspw', NULL, NULL, NULL, MYSQLI_CLIENT_SSL);
php> var_dump(mysqli_query($db, 'STATUS;'));  # Should show results if connection succeeded, not false.
```

```
210128 20:55:44      4 Connect  wordpress2@172.24.26.1 on  using SSL/TLS
210128 20:55:54      4 Query  STATUS
```




# PHP-FPM and Wordpress and NginX
Facts:
- The wordpress site files have to be mounted to both php-fpm and nginx container
- The path of the mounted site has to be the same, this is due to the FastCGI_params passing from NginX.

`docker-compose run --no-deps --rm --entrypoint "bash" phpsite1` to debug.

WARNING the nginxconf.io comes with a defaut cgi_pass directive!

### PROBLEM 01 - the pools:
1. Clear all the default pool confs
result: container quits with no error
RUN rm /usr/local/etc/php-fpm.d/*
COPY ./phpsite.conf /usr/local/etc/php-fpm.d
2. Instead delete the www.conf pool. 
result fails as[pool www] has not been defined.
RUN rm /usr/local/etc/php-fpm.d/www.conf
COPY ./phpsite.conf /usr/local/etc/php-fpm.d
3. Copy in new www.conf binding to sock, and not 9000. 
result: Only 502 errors on frontent with file not found php-fpm error on backend.
COPY ./www.conf /usr/local/etc/php-fpm.d

SOLUTION, which does not make any sense:
To make the site work wave to include both a www pool and a custom pool, both the following examples work.

```
# (bind to sock(custom pool) and port 9000(www pool)
PID   USER     TIME  COMMAND
    1 root      0:00 php-fpm: master process (/usr/local/etc/php-fpm.conf)
   71 www-data  0:00 php-fpm: pool www
   72 www-data  0:00 php-fpm: pool www
   73 www-data  0:00 php-fpm: pool phpsite
   75 www-data  0:00 bash
   80 www-data  0:00 ps -a

# (bind to sock(custom pool) and to same sock(www pool)
PID   USER     TIME  COMMAND
    1 root      0:00 php-fpm: master process (/usr/local/etc/php-fpm.conf)
   71 www-data  0:00 php-fpm: pool www
   72 www-data  0:00 php-fpm: pool www
   73 www-data  0:00 bash
   78 www-data  0:00 ps -a
```

### PROBLEM 02 - the source files:
The fact that the source files path/location has to be identical in nginx and in the php-fpm container. This is a ral problem as we do spwan multiple php-fpm instances but use only one nginx one, which means the nginx does need multiple source file locations.

A solution is to pass the modified path
`fastcgi_param       SCRIPT_FILENAME   /var/www/html/$fastcgi_script_name;`

This is the same problem:
https://stackoverflow.com/questions/29905953/how-to-correctly-link-php-fpm-and-nginx-docker-containers#32999924
https://www.nginx.com/resources/wiki/start/topics/examples/phpfcgi/




# Docker and docker-compose details

## Ports in containers
Please be aware of the differences if going to production, which is when the mariadb and wordpress containers don't need port mappings to the host. They will only need to be accessed over the docker network.

## Restart policy
off (Default) A container will not be restarted automatically
on-failure	A container will restart automatically after a failure. A limit can be applied by appending :<int> to the end. For example, limiting restarts to 5 failures only we would use on-failure:5.
unless-stopped	A container will always restart automatically unless it has been manually stopped.
always	The container will always restart itself

## Networks
Due to some machines running VPN, or the existence of other docker infrastructure, it is recommended to create an independent subnet for this project. Before running `docker-compose up`make sure that the network exists on your host machine, which is what the command `docker network create net-1 --subnet 172.24.26.0/24` does. Feel free to cahnge the subnet, it's the `net-1` name which is important.

## Cgroups
As the move to cgroups v2 isn't complete on many Linux distros at time of writing January 2020, there may be an error when launching docker containers. Here's a workaround for now, but come end-January 2021 reports say it sould be back to normal.

```
sudo mkdir /sys/fs/cgroup/systemd
sudo mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd
```




# NginX
Todo: WORK IN PROGRESS


## NginX configuration
This is generated from the helper at [Nginxconf.io at Digital Ocean](https://www.digitalocean.com/community/tools/nginx). See note in DH params

Each site needs a configuration file in `conf/nginx/sites-enabled`, which is then volume mounted to the NginX container's `sites-enabled`. This is not the only alternative: files in `sites-availalble` can instead be symlinked to `sites-enabled` on the host. `ln -s ./sites-available/wordpress1.conf ./sites-enabled/wordpress1.cof`. The first option is what's used here.

Loging levels for error_log
emerg: Emergency messages when your system may be unstable.
alert: Alert messages of serious issues.
crit: Critical issues that need to be taken care of immediately.
error: An error has occured. Something went wrong while processing a page.
warn: A warning messages that you should look into it.
notice: A simple log notice that you can ignore.
info: Just an information messages that you might want to know.
debug: Debugging information used to pinpoint the location of error.

## NginX as reverse proxy
This project does use nginx as reverse proxy.

## NginX cache
It is possibly a good idea to save the cache to a volume mount so that it is available on restarts - however for initial development we don't. See compose file for that option.



# Debugging and problems
The docker-compose includes a service called debugger which after `docker-compose up --build` you can enter with `docker exec -it debugger bash` and run various tests to see if things are working. 

For example `mysql -u wordpress1 --port=3306 --host=mariadb -p` should prompt for password (default is "wordpress") and then you should have a connection to the database.


## Solution: removing the database (DANGEROUS)
From host machine try deleting the entirety of the database: `sudo rm -r data/mariadb/*`. The database is rirrevokably lost (so make sure backups have been made).

## Solution: removing the wordpress site
`sudo rm -r wordpress1/*`

## Solution: magic Docker commands
`docker container prune`, `docker network prune`, and the likes can be the saviour if things are not behaving normally.


## DNS and name resoultion on Docker networks
Soemtimes NginX, or a PHP process will not be able to resolve names of other containers on the network. IT can be neccessary to give a custom DNS server. 120.0.0.11 is where Docker's internal DNS runs, which can be given as an instruction. In NginX its `resolver 127.0.0.11 valid=10s ipv6=off;` for example.
      


# Details on this repository

## Cloning or pulling submodules
Either initially: `git clone --recursive <url>`, or after cloning main repo:

```
git clone <url>
cd <repo>
git submodule update --init
```

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3, see LICENSE for more information.